package com.mobileenerlytics.eagle.tester.logger;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static android.os.Looper.myLooper;

/**
 * Main class for performing battery measurements.
 *
 * @author Abhilash Jindal
 */
public class EagleTester {
    // These values must match with MessageService
    private static final int MSG_STOP = 0;
    private static final int MSG_START = 1;
    private static final int MSG_STOPPED_EPROF = 2;
    private static final int MSG_STARTED_EPROF = 3;
    private static final String LOGNAME = "tester_logname";
    private static final int ARG_FAILED=1;

    private String packageName = "";
    private final String PACKAGE_KEY = "packageName";
    private static final String TAG = "EagleTester";
    private static final String className = "EagleTester";
    private static final String APP_PKG_NAME = "com.mobileenerlytics.eagle.tester.app";
    private static final String APP_SERVICE_NAME = "com.mobileenerlytics.eagle.tester.app.MessageService";
    private final Context mContext;
    MyServiceConnection serviceConnection;
    private String startedTest;
    private Set<String> finishedTests = new HashSet<>();

    /**
     * Messenger for communicating with the service.
     */
    Messenger mService = null;

    /**
     * Latch to wait for connection with LoggerService.
     */
    private CountDownLatch serviceLatch;

    /**
     * Latch to wait for LooperThread to init messenger.
     */
    private CountDownLatch messengerLatch;

    /**
     * Latches to wait for starting, stopping eprof
     */
    private CountDownLatch startEprofLatch;
    private CountDownLatch stopEprofLatch;
    private static final int TIMEOUT_M = 1;    // 1 minutes

    // Allow exactly one startMeasure to be in progress across all instances of EagleTester classes.
    // Must be kept static!
    private final static Semaphore profilingSemaphore = new Semaphore(1);

    /**
     * Handler of incoming messages from service.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_STOPPED_EPROF:
                    Log.i(TAG, "Stopped eprof");
                    if (stopEprofLatch != null)
                        stopEprofLatch.countDown();
                    else
                        Log.w(TAG, "stopEprofLatch is null");
                    break;
                case MSG_STARTED_EPROF:
                    Log.i(TAG, "Started eprof");
                    if (startEprofLatch != null)
                        startEprofLatch.countDown();
                    else
                        Log.w(TAG, "startEprofLatch is null");
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Messenger that forwards data from Service to IncomingHandler. This is made volatile
     * since it is init in LooperThread, but used in this thread.
     */
    private volatile Messenger incomingMessenger;

    class MyServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mService = new Messenger(service);
            serviceLatch.countDown();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
        }
    }

    class LooperThread extends Thread {
        @Override
        public void run() {
            Looper.prepare();
            incomingMessenger = new Messenger(new IncomingHandler());
            messengerLatch.countDown();
            Looper.loop();
        }

        @Override
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        public void interrupt() {
            Looper looper = myLooper();
            if (looper != null)
                looper.quitSafely();
            incomingMessenger = null;
            super.interrupt();
        }
    }

    Thread looperThread;

    /** @hide */
    @Deprecated
    public EagleTester(Context context) throws InterruptedException {
        messengerLatch = new CountDownLatch(1);
        looperThread = new LooperThread();
        looperThread.start();
        messengerLatch.await();

        mContext = context.getApplicationContext();
        serviceConnection = new MyServiceConnection();
        serviceLatch = new CountDownLatch(1);
        Intent intent = new Intent();
        intent.setClassName(APP_PKG_NAME, APP_SERVICE_NAME);
        mContext.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE | Context.BIND_IMPORTANT);
    }

    public EagleTester(Context context, String packageName) throws InterruptedException {
        this.packageName = packageName;
        messengerLatch = new CountDownLatch(1);
        looperThread = new LooperThread();
        looperThread.start();
        messengerLatch.await();

        mContext = context.getApplicationContext();
        serviceConnection = new MyServiceConnection();
        serviceLatch = new CountDownLatch(1);
        Intent intent = new Intent();
        intent.setClassName(APP_PKG_NAME, APP_SERVICE_NAME);
        mContext.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE | Context.BIND_IMPORTANT);
    }

    /**
     * This method should be called after battery tests have finished.
     *
     * @throws InterruptedException if any thread has interrupted the current thread.
     */
    public void finish() throws InterruptedException {
        Log.i(TAG, "finish");
        boolean acquired = false;
        do {
            acquired = profilingSemaphore.tryAcquire(5, TimeUnit.SECONDS);
            if (!acquired) {
                // If a test crashes between startMeasure and stopMeasure, stopMeasure never gets called!
                // If we see a finish while a test is in progress, the test must have a
                // crashed; we call stopMeasure here.
                Log.w(TAG, "Tried to finish but measurement for " + startedTest
                        + " is already in progress. Stopping " + startedTest);
                stopMeasure_(startedTest, false);
            }
        } while (!acquired);
        // Semaphore is now acquired

        mContext.unbindService(serviceConnection);
        looperThread.interrupt();
    }


    /**
     * Start energy measurement.
     *
     * @param name name of the test case.
     * @throws InterruptedException if any thread has interrupted the current thread.
     */
    public void startMeasure(String name) throws InterruptedException {
        startMeasure(name, LoggedComponent.DEFAULT);
    }

    /**
     * Start energy measurement with passed features disabled.
     *
     * @param name name of the test case.
     * @param disabledComponents components that shall be disabled for this test.
     * */
    public void startMeasure(String name, LoggedComponent... disabledComponents) throws InterruptedException {
        Log.i(TAG, "Start measuring energy");
        Log.d(TAG, name);

        if (mService == null) {
            // Wait to connect to the service
            boolean connected = serviceLatch.await(5, TimeUnit.SECONDS);
            if(!connected) {
                String msg = "Failed to connect to Measurement Service. Please " +
                        "check if Eagle Tester app is indeed installed!";
                Log.e(TAG, msg);
                throw new RuntimeException(msg);
            }
        }

        if (startedTest != null && startedTest.equals(name)) {
            String msg = "startMeasure was already called for " + name;
            Log.e(TAG, msg);
            throw new AssertionError(msg);
        }

        if (finishedTests.contains(name)) {
            String msg = "Each testcase must have a unique name. Energy consumption for "
                    + name + " was already measured for this build";
            Log.e(TAG, msg);
            throw new UnsupportedOperationException(msg);
        }

        boolean acquired = false;
        do {
            acquired = profilingSemaphore.tryAcquire(5, TimeUnit.SECONDS);
            if (!acquired) {
                // If a test crashes between startMeasure and stopMeasure, stopMeasure never gets called!
                // This hangs the test that follows.
                // Hence if we see 2 consecutive startMeasure, then the first one must have
                // crashed; we call stopMeasure for the first test here.
                Log.w(TAG, "Tried to startMeasure " + name + " but measurement for " + startedTest
                        + " is already in progress. Stopping " + startedTest);
                stopMeasure_(startedTest, false);
            }
        } while (!acquired);

        // Semaphore is now acquired
        startedTest = name;
        Message message = Message.obtain(null, MSG_START, 0, 0);
        message.replyTo = incomingMessenger;
        Bundle bundle = new Bundle();
        bundle.putString(LOGNAME, name);
        bundle.putString(PACKAGE_KEY, packageName);
        message.setData(bundle);
        message.arg1 = LoggedComponent.getMap(disabledComponents);
        try {
            startEprofLatch = new CountDownLatch(1);
            mService.send(message);
            // Wait for eprof to start
            if(!startEprofLatch.await(TIMEOUT_M, TimeUnit.MINUTES)) {
                Log.w(TAG, "Start energy logging timed out. Returning to the battery test " +
                        "without fulling starting logging.");
            }
            startEprofLatch = null;
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void stopMeasure_(String name, boolean passed) {
        Log.i(TAG, "Stop measuring energy");
        Log.d(TAG, name);

        if (finishedTests.contains(name)) {
            Log.w(TAG, "Test " + name + " was already stopped. This may have been in error and " +
                    "can cause incorrect energy measurement. Please make sure that there is only one " +
                    "energy measurement in progress at once.");
            return;
        }

        if (!name.equals(startedTest)) {
            String msg = String.format("Called stopMeasure on %s, but the energy measurement is in " +
                    "progress for %s. Please make sure the names in startMeasure and stopMeasure " +
                    "match for the same test case.", name, startedTest);
            Log.e(TAG, msg);
            throw new AssertionError(msg);
        }

        finishedTests.add(name);
        int arg = passed ? 0 : ARG_FAILED;
        Message message = Message.obtain(null, MSG_STOP, arg, 0);
        message.replyTo = incomingMessenger;
        try {
            stopEprofLatch = new CountDownLatch(1);
            mService.send(message);

            // Wait for eprof to stop
            if(!stopEprofLatch.await(TIMEOUT_M, TimeUnit.MINUTES)) {
                Log.w(TAG, "Stop energy logging timed out. Returning to the battery test " +
                        "without fulling stopping logging.");
            }
            stopEprofLatch = null;
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        profilingSemaphore.release();
    }


    /**
     * Stop energy measurement.
     *
     * @param name name of the test case. This name must match the name passed to startMeasure(name).
     */
    public void stopMeasure(String name) {
        stopMeasure_(name, true);
    }

}
