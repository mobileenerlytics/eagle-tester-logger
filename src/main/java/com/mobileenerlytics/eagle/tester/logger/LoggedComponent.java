package com.mobileenerlytics.eagle.tester.logger;

/**
 * Components that're logged while running a test.
 */
public enum LoggedComponent {
    // These must match the enum in EnergyLogger
    CPU (0x1),
    GPU (0x2),
    NET (0x4),
    GPS (0x8),
    POWER_EVENT (0x10),
    SCREEN_VIDEO (0x20),
    LOGCAT (0x40),
    BUGREPORT(0x80),
    STRACE (0x100),
    DEFAULT(0x180), // Disable BUGREPORT and STRACE by default
    ALL (0x0);
    int bitmap;

    LoggedComponent(int bitmap) {
        this.bitmap = bitmap;
    }

    /** @hide */
    static int getMap(LoggedComponent... logs) {
        // disable the experimental feature BUGREPORT for now
        int ret = LoggedComponent.BUGREPORT.bitmap;
        for (LoggedComponent log: logs) {
            ret |= log.bitmap;
        }
        return ret;
    }
}
